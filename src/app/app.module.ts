import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WelcomeComponent } from './home/welcome.component';

import { AppComponent }  from './app.component';
import { MenuComponent }  from './menu.component';
import { CustomersModule } from './customers/customers.module';
import { BillingModule } from './billing/billing.module';
import { UsersModule } from './users/users.module';
import { DataService } from './data.service';
import { ActionsModule } from './actions/actions.module';


@NgModule({
  imports: [
      BrowserModule,
      HttpModule,      
      CommonModule,
      FormsModule,
            
    RouterModule.forRoot([
      
        { path: 'welcome', component: WelcomeComponent },
        { path: '', redirectTo: 'welcome', pathMatch: 'full' },
        { path: '**', redirectTo: 'welcome', pathMatch: 'full' }
    ]),

      CustomersModule,
      BillingModule,
      UsersModule,
      ActionsModule
    ],

  providers: [DataService],

  declarations: [
      AppComponent,
      MenuComponent,    
      WelcomeComponent      
  ],
  bootstrap: [AppComponent, MenuComponent]
})
export class AppModule { }


