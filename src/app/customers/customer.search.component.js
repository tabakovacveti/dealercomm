"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var script_service_1 = require("../script.service");
var data_service_1 = require("../data.service");
var CustomerSearchComponent = (function () {
    function CustomerSearchComponent(dataService) {
        this.dataService = dataService;
        this.pageTitle = 'Customer Detail';
        this.currentProgress = 0;
        this.scriptService = new script_service_1.ScriptService();
        this.customersList = [];
        this.scriptService.load('table-datatables-rowreorder', 'datatable', 'datatable-plugin', 'daterangepicker').then(function (data) {
            // console.log('script loaded ', data);
        }).catch(function (error) { return console.log(error); });
    }
    CustomerSearchComponent.prototype.getCustomers = function () {
        this.customersList = this.dataService.getCustomers();
        console.log("customer search");
        console.log(this.customersList);
    };
    CustomerSearchComponent.prototype.ngOnInit = function () {
        this.getCustomers();
    };
    CustomerSearchComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/customers/customer.search.component.html',
        }),
        __metadata("design:paramtypes", [data_service_1.DataService])
    ], CustomerSearchComponent);
    return CustomerSearchComponent;
}());
exports.CustomerSearchComponent = CustomerSearchComponent;
//# sourceMappingURL=customer.search.component.js.map