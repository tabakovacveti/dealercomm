import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription }       from 'rxjs/Subscription';

import { Customer } from './customer';
import { CustomerService } from './customer.service';

@Component({
    templateUrl: 'business.customer.component.html'
})
export class BusinessCustomerComponent implements OnInit, OnDestroy {
    pageTitle: string = 'Business Customer Detail';
    customer: Customer;
    errorMessage: string;
    private sub: Subscription;

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _customerService: CustomerService) { 
    }

    ngOnInit(): void {
        this.sub = this._route.params.subscribe(
            params => {
                let id = +params['id'];
                this.getCustomer(id);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    getCustomer(id: number) {
        this._customerService.getCustomer(id).subscribe(
            customer => this.customer = customer,
            error => this.errorMessage = <any>error);
    }

    onBack(): void {
        this._router.navigate(['/customers']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Product Detail: ' + message;
    }
}
