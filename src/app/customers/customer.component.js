"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var customer_1 = require("./customer");
var data_service_1 = require("../data.service");
var CustomerComponent = (function () {
    function CustomerComponent(dataService) {
        this.dataService = dataService;
        this.pageTitle = 'Customer Detail';
        this.customersList = [];
        this.customer = new customer_1.Customer();
        this.currentProgress = 0;
    }
    CustomerComponent.prototype.getCustomers = function () {
        this.customersList = this.dataService.getCustomers();
        console.log("customer componet");
        console.log(this.customersList);
    };
    CustomerComponent.prototype.cancel = function () {
        this.clearCustomer();
    };
    CustomerComponent.prototype.saveCustomer = function () {
        this.customer.type = "Private";
        this.dataService.addCustomer(this.customer);
        this.clearCustomer();
    };
    CustomerComponent.prototype.ngOnInit = function () {
        this.getCustomers();
    };
    CustomerComponent.prototype.clearCustomer = function () {
        this.customer = new customer_1.Customer();
    };
    CustomerComponent.prototype.calcProgress = function () {
        var current = 0;
        if (!this.checkIsEmpty(this.customer.id))
            current += 10;
        if (!this.checkIsEmpty(this.customer.firstName))
            current += 10;
        if (!this.checkIsEmpty(this.customer.lastName))
            current += 10;
        if (!this.checkIsEmpty(this.customer.email))
            current += 10;
        if (!this.checkIsEmpty(this.customer.age))
            current += 10;
        if (!this.checkIsEmpty(this.customer.country))
            current += 10;
        if (!this.checkIsEmpty(this.customer.address1))
            current += 10;
        if (!this.checkIsEmpty(this.customer.address2))
            current += 10;
        if (!this.checkIsEmpty(this.customer.zipCode))
            current += 10;
        if (!this.checkIsEmpty(this.customer.gender))
            current += 10;
        if (!this.checkIsEmpty(this.customer.mobile))
            current += 10;
        if (!this.checkIsEmpty(this.customer.contacts))
            current += 10;
        if (!this.checkIsEmpty(this.customer.state))
            current += 10;
        if (current > 100)
            current = 100;
        this.currentProgress = current;
        if (current > 0)
            $("#customerEditMode").show();
        else
            $("#customerEditMode").hide();
        return current;
    };
    CustomerComponent.prototype.checkIsEmpty = function (value) {
        if (value == "" || value == undefined || value == null)
            return true;
        return false;
    };
    CustomerComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/customers/customer.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService])
    ], CustomerComponent);
    return CustomerComponent;
}());
exports.CustomerComponent = CustomerComponent;
//# sourceMappingURL=customer.component.js.map