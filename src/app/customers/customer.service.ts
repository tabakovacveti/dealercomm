import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Customer } from './customer';

@Injectable()
export class CustomerService {
    private _customerUrl = 'api/customer/customers.json';

    constructor(private _http: Http) { }

    getCustomers(): Observable<Customer[]> {
        return this._http.get(this._customerUrl)
            .map((response: Response) => <Customer[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    getCustomer(id: number): Observable<Customer> {
        return this.getCustomers()
            .map((customers: Customer[]) => customers.find(p => p.id === id));
    }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
