import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Customer } from './customer';
import { CustomerService } from './customer.service';
import { DataService } from '../data.service';

declare var $: any;

@Component({

    templateUrl: '/customer.component.html'
})
export class CustomerComponent { 
    pageTitle: string = 'Customer Detail';
      
    public customersList: Customer[] = [];

    constructor(private dataService: DataService) {}

    getCustomers() {
        this.customersList = this.dataService.getCustomers();
        console.log("customer componet");
        console.log(this.customersList);
    }

    customer: Customer = new Customer();
    currentProgress: number = 0;
   
    cancel(): void {
        
        this.clearCustomer();
    }

    saveCustomer(): void {

        this.customer.type = "Private";
        this.dataService.addCustomer(this.customer);
        this.clearCustomer();
    }

    ngOnInit(): void {

        this.getCustomers();  
    }

    
    clearCustomer() {
        this.customer = new Customer();
    }

    calcProgress(): number {

        var current: number = 0;
        
        if (!this.checkIsEmpty(this.customer.id))
            current += 10;
        if (!this.checkIsEmpty(this.customer.firstName))
            current += 10;
        if (!this.checkIsEmpty(this.customer.lastName))
            current += 10;
        if (!this.checkIsEmpty(this.customer.email))
            current += 10;
        if (!this.checkIsEmpty(this.customer.age))
            current += 10;
        if (!this.checkIsEmpty(this.customer.country))
            current += 10;
        if (!this.checkIsEmpty(this.customer.address1))
            current += 10;
        if (!this.checkIsEmpty(this.customer.address2))
            current += 10;
        if (!this.checkIsEmpty(this.customer.zipCode))
            current += 10;
        if (!this.checkIsEmpty(this.customer.gender))
            current += 10;
        if (!this.checkIsEmpty(this.customer.mobile))
            current += 10;
        if (!this.checkIsEmpty(this.customer.contacts))
            current += 10;
        if (!this.checkIsEmpty(this.customer.state))
            current += 10;

        if (current > 100)
            current = 100;

        this.currentProgress = current;

        if (current > 0)
            $("#customerEditMode").show();
        else
            $("#customerEditMode").hide();

        return current;
    }

    checkIsEmpty(value: any): boolean {

        if (value == "" || value == undefined || value == null)
            return true;

        return false;
    }           
}
