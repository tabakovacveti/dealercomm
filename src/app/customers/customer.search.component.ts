import { Component, OnInit, OnDestroy, Input, Directive} from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Subscription }       from 'rxjs/Subscription';
import { ScriptService } from '../script.service';
import { Customer } from './customer';
import { CustomerService } from './customer.service';
import { DataService } from '../data.service';


@Component({
    templateUrl: '/customer.search.component.html',      
})
export class CustomerSearchComponent { 
    pageTitle: string = 'Customer Detail';
       
    currentProgress: number = 0;    
    scriptService: ScriptService = new ScriptService();
    
    public customersList: Customer[] = [];  
    
    constructor(private dataService: DataService) {
        
          
    }

    getCustomers() {
        this.customersList = this.dataService.getCustomers();

        console.log("customer search");
        console.log(this.customersList);
    }

    ngOnInit(): void {

        this.getCustomers();        
    }    
    
}
