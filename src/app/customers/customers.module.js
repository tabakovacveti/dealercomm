"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var customer_component_1 = require("./customer.component");
var customer_filter_pipe_1 = require("./customer-filter.pipe");
var business_customer_component_1 = require("./business.customer.component");
var customer_search_component_1 = require("./customer.search.component");
var customer_service_1 = require("./customer.service");
var CustomersModule = (function () {
    function CustomersModule() {
    }
    CustomersModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                common_1.CommonModule,
                forms_1.FormsModule,
                router_1.RouterModule.forChild([
                    { path: 'customer', component: customer_component_1.CustomerComponent },
                    { path: 'customer-search', component: customer_search_component_1.CustomerSearchComponent },
                    { path: 'business-customer', component: business_customer_component_1.BusinessCustomerComponent },
                    { path: 'business-customer/:id', component: business_customer_component_1.BusinessCustomerComponent },
                    { path: 'customer/:id', component: customer_component_1.CustomerComponent }
                ])
            ],
            declarations: [
                customer_component_1.CustomerComponent,
                customer_search_component_1.CustomerSearchComponent,
                business_customer_component_1.BusinessCustomerComponent,
                customer_filter_pipe_1.CustomerFilterPipe
            ],
            providers: [
                customer_service_1.CustomerService
            ]
        })
    ], CustomersModule);
    return CustomersModule;
}());
exports.CustomersModule = CustomersModule;
//# sourceMappingURL=customers.module.js.map