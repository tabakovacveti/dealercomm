/* Defines the product entity */
export class Customer {
    id: number;
    type: string;
    firstName: string;
    lastName: string;
    title: string;
    email: string;
    age: string;
    country: string;
    address1: string;
    address2: string;
    zipCode: string;
    gender: string;
  
    mobile: string;
    contacts: string;
    state: string;
    fax: string;
}

