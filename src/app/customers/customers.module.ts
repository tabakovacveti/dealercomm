import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CustomerComponent } from './customer.component';
import { CustomerFilterPipe } from './customer-filter.pipe';
import { BusinessCustomerComponent } from './business.customer.component';
import { CustomerSearchComponent } from './customer.search.component';

import { CustomerService } from './customer.service';

@NgModule({
  imports: [

      BrowserModule,
      HttpModule,
      CommonModule,
      FormsModule,

    RouterModule.forChild([
          { path: 'customer', component: CustomerComponent }, 
          { path: 'customer-search', component: CustomerSearchComponent },
          { path: 'business-customer', component: BusinessCustomerComponent },
          { path: 'business-customer/:id', component: BusinessCustomerComponent },
          { path: 'customer/:id', component: CustomerComponent }
    ])
  ],
  declarations: [
      CustomerComponent,
      CustomerSearchComponent,
      BusinessCustomerComponent,
      CustomerFilterPipe     
  ],
  providers: [
    CustomerService
  ]
})
export class CustomersModule {}
