/* Defines the product entity */
export class User {
    id: number;    
    firstName: string;
    lastName: string;
    title: string;
    email: string;         
    mobile: string;    
    occupation: string;
    password: string;
}

