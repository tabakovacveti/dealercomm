import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UserComponent } from './user.component';
import { UserFilterPipe } from './user-filter.pipe';
import {PasswordModule} from 'primeng/primeng';
import { UserSearchComponent } from './user.search.component';

@NgModule({
  imports: [

      BrowserModule,
      HttpModule,
      CommonModule,
      FormsModule,
      PasswordModule,
    RouterModule.forChild([
          { path: 'user', component: UserComponent }, 
          { path: 'user-search', component: UserSearchComponent },
         
          { path: 'user/:id', component: UserComponent }
    ])
  ],
  declarations: [
      UserComponent,
      UserSearchComponent,      
      UserFilterPipe     
  ],
  providers: [
    
  ]
})
export class UsersModule {}
