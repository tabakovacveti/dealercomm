import {  PipeTransform, Pipe } from '@angular/core';
import { User } from './user';

@Pipe({
    name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {

    transform(value: User[], filterBy: string): User[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((user: User) =>
            user.firstName.toLocaleLowerCase().indexOf(filterBy) !== -1 || user.mobile.toLocaleLowerCase().indexOf(filterBy) !== -1 || user.occupation.toLocaleLowerCase().indexOf(filterBy) !== -1 || user.email.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}
