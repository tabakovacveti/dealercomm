import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { User } from './user';

import { DataService } from '../data.service';

declare var $: any;

@Component({

    templateUrl: '/user.component.html'
})
export class UserComponent { 
    pageTitle: string = 'Creat User';
      
    public usersList: User[] = [];

    constructor(private dataService: DataService) {}

    getUsers() {
        this.usersList = this.dataService.getUsers();
        console.log("user componet");
        console.log(this.usersList);
    }

    user: User = new User();
    currentProgress: number = 0;
   
    cancel(): void {
        
        this.clearUser();
    }

    saveUser(): void {       
        this.dataService.addUser(this.user);
        this.clearUser();
    }

    ngOnInit(): void {
        this.getUsers();  
    }
    
    clearUser() {
        this.user = new User();
    }

    //calcProgress(): number {

    //    var current: number = 0;
        
    //    if (!this.checkIsEmpty(this.customer.id))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.firstName))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.lastName))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.email))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.age))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.country))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.address1))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.address2))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.zipCode))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.gender))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.mobile))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.contacts))
    //        current += 10;
    //    if (!this.checkIsEmpty(this.customer.state))
    //        current += 10;

    //    if (current > 100)
    //        current = 100;

    //    this.currentProgress = current;

    //    if (current > 0)
    //        $("#customerEditMode").show();
    //    else
    //        $("#customerEditMode").hide();

    //    return current;
    //}

    checkIsEmpty(value: any): boolean {

        if (value == "" || value == undefined || value == null)
            return true;

        return false;
    }           
}
