import { Component, OnInit, OnDestroy, Input, Directive} from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Subscription }       from 'rxjs/Subscription';
import { ScriptService } from '../script.service';
import { User } from './user';
//import { CustomerService } from './customer.service';
import { DataService } from '../data.service';


@Component({
    templateUrl: '/user.search.component.html',      
})
export class UserSearchComponent { 
    pageTitle: string = 'Search User';
       
    currentProgress: number = 0;    
    scriptService: ScriptService = new ScriptService();
    
    public usersList: User[] = [];  
    
    constructor(private dataService: DataService) {              
    }

    getUsers() {
        this.usersList = this.dataService.getUsers();

        console.log("user search");
        console.log(this.usersList);
    }

    ngOnInit(): void {

        this.getUsers();        
    }    
    
}
