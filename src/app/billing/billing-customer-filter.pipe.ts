import {  PipeTransform, Pipe } from '@angular/core';
import { Customer } from '../customers/customer';

@Pipe({
    name: 'customerFilter'
})
export class BillingCustomerFilterPipe implements PipeTransform {

    transform(value: Customer[], filterBy: string): Customer[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((customer: Customer) =>
            customer.firstName.toLocaleLowerCase().indexOf(filterBy) !== -1 || customer.mobile.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}
