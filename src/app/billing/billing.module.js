"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var primeng_1 = require("primeng/primeng");
//import { CustomerComponent } from './customer.component';
//import { BusinessCustomerComponent } from './business.customer.component';
var invoice_search_component_1 = require("./invoice.search.component");
var invoice_component_1 = require("./invoice.component");
var billing_customer_filter_pipe_1 = require("./billing-customer-filter.pipe");
//import { CustomerService } from './customer.service';
var BillingModule = (function () {
    function BillingModule() {
    }
    BillingModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                common_1.CommonModule,
                forms_1.FormsModule,
                primeng_1.CalendarModule,
                router_1.RouterModule.forChild([
                    { path: 'invoice-search', component: invoice_search_component_1.InvoiceSearchComponent },
                    { path: 'invoice/:id', component: invoice_component_1.InvoiceComponent },
                    { path: 'invoice', component: invoice_component_1.InvoiceComponent },
                ])
            ],
            declarations: [
                //CustomerComponent,
                invoice_search_component_1.InvoiceSearchComponent,
                invoice_component_1.InvoiceComponent,
                billing_customer_filter_pipe_1.BillingCustomerFilterPipe
            ],
            providers: []
        })
    ], BillingModule);
    return BillingModule;
}());
exports.BillingModule = BillingModule;
//# sourceMappingURL=billing.module.js.map