/* Defines the product entity */
export class Invoice {
    id: number;
    firstName: string;
    lastName: string;
    invoiceDate: Date;
    mobile: string;
    total: number;
    customerType: string;
}

