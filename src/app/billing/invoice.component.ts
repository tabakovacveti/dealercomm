import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

import {MenuItem} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { ScriptService } from '../script.service';
import { Invoice } from './invoice';
import { Customer } from '../customers/customer';
//import { CustomerService } from './customer.service';
import { DataService } from '../data.service';

declare var $: any;

@Component({

    templateUrl: '/invoice.component.html'
})
export class InvoiceComponent {
    pageTitle: string = 'Generate Invoice';
    value: Date;

    public invoicesList: Invoice[] = [];
    scriptService: ScriptService = new ScriptService();

    public customersList: Customer[] = [];
    public onDemand: boolean = false;
    public showDetail: boolean = false;

    constructor(private dataService: DataService) {
       
    }

    changeInvoiceType(invoiceType: boolean): void {        
        this.onDemand = invoiceType;
    }

    generateInvoice(): void {
        this.showDetail = true;
    }

    getInvoices() {
        this.invoicesList = this.dataService.getInvoices();
        console.log("invoice component");
        console.log(this.invoicesList);
    }
    getCustomers() {
        this.customersList = this.dataService.getCustomers();

        console.log("customer search");
        console.log(this.customersList);
    }
 
    invoice: Invoice = new Invoice();
    currentProgress: number = 0;

    cancel(): void {

        this.clearInvoice();
    }

    saveInvoice(): void {

        this.dataService.addInvoice(this.invoice);
        this.clearInvoice();
    }

    ngOnInit(): void {
        this.getCustomers();
        this.getInvoices();
    }


    clearInvoice() {
        this.invoice = new Invoice();
    }

  
}
