import { Component, OnInit, OnDestroy, Input, Directive} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription }       from 'rxjs/Subscription';
import { ScriptService } from '../script.service';
import { Invoice } from './invoice';
//import { CustomerService } from './customer.service';
import { DataService } from '../data.service';


@Component({
    templateUrl: 'invoice.search.component.html',      
})
export class InvoiceSearchComponent  { 
    pageTitle: string = 'Invoice Search';
    private sub: Subscription;   
    //currentProgress: number = 0;    
    scriptService: ScriptService = new ScriptService();
    
    public invoicesList: Invoice[] = [];  
    
    constructor(private _route: ActivatedRoute,
        private _router: Router, private dataService: DataService
    ) {
        
    }
    getInvoice() {
        this.invoicesList = this.dataService.getInvoices();

        console.log("invoice search");
        console.log(this.invoicesList);
    }

    ngOnInit(): void {

        this.getInvoice();        
    }    
    //ngOnDestroy() {
    //    this.sub.unsubscribe();
    //}
    
}
