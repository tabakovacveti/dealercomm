"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var script_service_1 = require("../script.service");
var invoice_1 = require("./invoice");
//import { CustomerService } from './customer.service';
var data_service_1 = require("../data.service");
var InvoiceComponent = (function () {
    function InvoiceComponent(dataService) {
        this.dataService = dataService;
        this.pageTitle = 'Generate Invoice';
        this.invoicesList = [];
        this.scriptService = new script_service_1.ScriptService();
        this.customersList = [];
        this.onDemand = false;
        this.showDetail = false;
        this.invoice = new invoice_1.Invoice();
        this.currentProgress = 0;
        this.scriptService.load('daterangepicker', 'bootstrap-datepicker', 'bootstrap-timepicker', 'bootstrap-datetimepicker', 'components-date-time-pickers').then(function (data) {
            console.log('script loaded ', data);
        }).catch(function (error) { return console.log(error); });
    }
    InvoiceComponent.prototype.changeInvoiceType = function (invoiceType) {
        this.onDemand = invoiceType;
    };
    InvoiceComponent.prototype.generateInvoice = function () {
        this.showDetail = true;
    };
    InvoiceComponent.prototype.getInvoices = function () {
        this.invoicesList = this.dataService.getInvoices();
        console.log("invoice component");
        console.log(this.invoicesList);
    };
    InvoiceComponent.prototype.getCustomers = function () {
        this.customersList = this.dataService.getCustomers();
        console.log("customer search");
        console.log(this.customersList);
    };
    InvoiceComponent.prototype.cancel = function () {
        this.clearInvoice();
    };
    InvoiceComponent.prototype.saveInvoice = function () {
        this.dataService.addInvoice(this.invoice);
        this.clearInvoice();
    };
    InvoiceComponent.prototype.ngOnInit = function () {
        this.getCustomers();
        this.getInvoices();
    };
    InvoiceComponent.prototype.clearInvoice = function () {
        this.invoice = new invoice_1.Invoice();
    };
    InvoiceComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/billing/invoice.component.html'
        }),
        __metadata("design:paramtypes", [data_service_1.DataService])
    ], InvoiceComponent);
    return InvoiceComponent;
}());
exports.InvoiceComponent = InvoiceComponent;
//# sourceMappingURL=invoice.component.js.map