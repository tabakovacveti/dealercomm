import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {CalendarModule} from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { CustomerComponent } from './customer.component';
//import { BusinessCustomerComponent } from './business.customer.component';

import { InvoiceSearchComponent } from './invoice.search.component';
import { InvoiceComponent } from './invoice.component';
import { BillingCustomerFilterPipe } from './billing-customer-filter.pipe';
//import { CustomerService } from './customer.service';

@NgModule({
  imports: [

      BrowserModule,
      HttpModule,
      CommonModule,
      FormsModule,
      CalendarModule,
      BrowserAnimationsModule,

    RouterModule.forChild([
        { path: 'invoice-search', component: InvoiceSearchComponent },
        { path: 'invoice/:id', component: InvoiceComponent },
        { path: 'invoice', component: InvoiceComponent },
       
    ])
  ],
  declarations: [
      //CustomerComponent,
      InvoiceSearchComponent,
      InvoiceComponent,
      BillingCustomerFilterPipe
  ],
  providers: [   
  ]
})
export class BillingModule {}
