import { Component, OnInit, OnDestroy, Input, Output, Injectable } from '@angular/core';
import { Customer } from './customers/customer';
import { User } from './users/user';
import { Invoice } from './billing/invoice';

@Injectable()
export class DataService {

    private customersList$: Customer[] = [];
    private usersList$: User[] = [];
    private invoicesList$: Invoice[] = [];

    constructor() {          
    }

    getCustomers() {
        return this.customersList$;
    }

    getUsers() {
        return this.usersList$;
    }

    addUser(user: User) {
        this.usersList$.push(user);
    }

    addCustomer(customer: Customer) {
        this.customersList$.push(customer);
    }
    getInvoices() {
        return this.invoicesList$;
    }

    addInvoice(invoice: Invoice) {
        this.invoicesList$.push(invoice);
    }
}
