import { Component } from '@angular/core';
import { Customer } from './customers/customer';
import { Invoice } from './billing/invoice';
import { User } from './users/user';
import { DataService } from './data.service';

@Component({
    selector: 'app-root',
    template: `   
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">  
                <router-outlet></router-outlet>            
            </div>
        </div>
        <div class="page-footer">
            <div class="page-footer-inner">
                2017 &copy; Dealer Commissioning

            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>               
   `
})

export class AppComponent {
    pageTitle: string = 'Dealer Commissioning';

    isAppLoaded: boolean = false;

    public customersList: Customer[] = [];
    
        
    constructor(private dataService: DataService) { } 

    addCustomers() {
        var customer1: Customer = new Customer();
        customer1.id = 20112222;
        customer1.type = "Business";
        customer1.firstName = "Ivan";
        customer1.lastName = "Ivanov";
        customer1.title = "Mr.";
        customer1.country = "BG";
        customer1.state = "Sofia";
        customer1.zipCode = "1000";
        customer1.mobile = "+35988566777";
        customer1.fax = "+359444212";
        customer1.email = "test@test.com";

        var customer2: Customer = new Customer();
        customer2.id = 7705220000;
        customer2.type = "Private";
        customer2.firstName = "Petar";
        customer2.lastName = "Petrov";
        customer2.title = "Mr.";
        customer2.country = "BG";
        customer2.state = "Burgas";
        customer2.zipCode = "4000";
        customer2.mobile = "+35988566333";
        customer2.fax = "+359444212";
        customer2.email = "customer@test.com";

        this.dataService.addCustomer(customer1);
        this.dataService.addCustomer(customer2);
    }

    addInvoices() {
        var invoice1: Invoice = new Invoice();
        invoice1.id = 333444111;
        invoice1.firstName = "Ivan";
        invoice1.lastName = "Ivanov";
        invoice1.invoiceDate = new Date();
        invoice1.mobile = "+359883888333";
        invoice1.total = 334;
        invoice1.customerType = "Business";
        this.dataService.addInvoice(invoice1);        
    }

    addUsers() {
        var user1: User = new User();
        user1.id = 333444111;
        user1.firstName = "Ivan";
        user1.lastName = "Ivanov";
        user1.email = "ivan@ivanov.com"
        user1.mobile = "+359883888333";
        user1.occupation = "Sales";
        
        this.dataService.addUser(user1);
    }

    ngOnInit(): void {

        this.addCustomers();   
        this.addInvoices();     
        this.addUsers(); 

        this.isAppLoaded = true;

       
    }
}
