import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { MassAction } from './massaction';

import { DataService } from '../data.service';

declare var $: any;

@Component({

    templateUrl: '/change.tariff.plan.component.html'
})
export class ChangeTariffPlanComponent { 
    pageTitle: string = 'Change Tariff Plan';
    ngOnInit(): void {
        $(".mask_phone").inputmask("mask", {
            "mask": "(999) 999-9999"
        }); //specifying fn & options
        $(".mask_number").inputmask({
            "mask": "9",
            "repeat": 10,
            "greedy": false
        }); // ~ mask "9" or mask "99" or ... mask "9999999999"

    }
}
