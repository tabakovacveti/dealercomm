import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { MassAction } from './massaction';

import { DataService } from '../data.service';

declare var $: any;

@Component({

    templateUrl: '/deactivate.card.component.html'
})
export class DeactivateCardComponent { 
    pageTitle: string = 'Deactivate Sim Cards';
          
    constructor(private dataService: DataService) { } 

    ngOnInit(): void {
        $(".mask_phone").inputmask("mask", {
            "mask": "(999) 999-9999"
        }); //specifying fn & options
        $(".mask_number").inputmask({
            "mask": "9",
            "repeat": 10,
            "greedy": false
        }); // ~ mask "9" or mask "99" or ... mask "9999999999"

    }
}
