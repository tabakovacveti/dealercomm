import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DeactivateCardComponent } from './deactivate.card.component';
import { ChangeTariffPlanComponent } from './change.tariff.plan.component';
import {PasswordModule} from 'primeng/primeng';

@NgModule({
  imports: [

      BrowserModule,
      HttpModule,
      CommonModule,
      FormsModule,
      PasswordModule,
    RouterModule.forChild([
        { path: 'change-tariff-plans', component: ChangeTariffPlanComponent }, 
        { path: 'deactivate-cards', component: DeactivateCardComponent }
                  
    ])
  ],
  declarations: [
      ChangeTariffPlanComponent,
      DeactivateCardComponent           
  ],
  providers: [
    
  ]
})
export class ActionsModule {}
