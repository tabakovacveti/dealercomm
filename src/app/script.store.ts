interface Scripts {
    name: string;
    src: string;
}
export const ScriptStore: Scripts[] = [
   
    { name: 'jquery',src: 'src/assets/global/plugins/jqueryjquery.min.js'},    
    { name: 'bootstrap',  src: 'src/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript' },
    { name: 'cookie', src: 'src/assets/global/plugins/js.cookie.min.js" type="text/javascript' },
    { name: 'hoover-dropdown', src: 'src/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js' },
    { name: 'slimscroll', src: 'src/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' },
    { name: 'blockui', src: 'src/assets/global/plugins/jquery.blockui.min.js'},
    { name: 'uniform', src: 'src/assets/global/plugins/uniform/jquery.uniform.min.js' },
    { name: 'bootstrap-switch', src: 'src/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' },
    { name: 'dashboard', src: 'src/assets/pages/scripts/dashboard.js' },
    

    { name: 'moment', src: 'src/assets/global/plugins/moment.min.js' },
    { name: 'morris', src: 'src/assets/global/plugins/morris/morris.min.js' },
    { name: 'raphael-min', src: 'src/assets/global/plugins/morris/raphael-min.js' },
    { name: 'waypoints', src: 'src/assets/global/plugins/counterup/jquery.waypoints.min.js' },
    { name: 'counterup', src: 'src/assets/global/plugins/counterup/jquery.counterup.min.js' },
    { name: 'serial', src: 'src/assets/global/plugins/amcharts/amcharts/serial.js' },
    { name: 'pie', src: 'src/assets/global/plugins/amcharts/amcharts/pie.js' },
    { name: 'radar', src: 'src/assets/global/plugins/amcharts/amcharts/radar.js' },
    { name: 'patterns', src: 'src/assets/global/plugins/amcharts/amcharts/themes/patterns.js' },
    { name: 'chalk', src: 'src/assets/global/plugins/amcharts/amcharts/themes/chalk.js' },
    { name: 'ammap', src: 'src/assets/global/plugins/amcharts/ammap/ammap.js' },
    { name: 'worldLow', src: 'src/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js' },
    { name: 'amstock', src: 'src/assets/global/plugins/amcharts/amstockcharts/amstock.js' },
    { name: 'flot', src: 'src/assets/global/plugins/flot/jquery.flot.min.js' },
    { name: 'resize', src: 'src/assets/global/plugins/flot/jquery.flot.resize.min.js' },
    { name: 'categories', src: 'src/assets/global/plugins/flot/jquery.flot.categories.min.js' },
    { name: 'sparkline', src: 'src/assets/global/plugins/jquery.sparkline.min.js' },
    { name: 'vmap', src: 'src/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js' },
    { name: 'app', src: 'src/assets/global/scripts/app.js' },
    { name: 'vmap-world', src: 'src/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js' },
    { name: 'daterangepicker', src: 'src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js' },  
    { name: 'bootstrap-datepicker', src: 'src/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' }, 
    { name: 'bootstrap-timepicker', src: 'src/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' },
    { name: 'bootstrap-datetimepicker', src: 'src/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js' },       
    { name: 'clockface', src: 'src/assets/global/plugins/clockface/js/clockface.js' }, 

    { name: 'components-date-time-pickers', src: 'src/assets/pages/scripts/components-date-time-pickers.min.js' }, 
   
    { name: 'table-datatables-rowreorder', src: 'src/assets/pages/scripts/table-datatables-rowreorder.min.js' },
    { name: 'datatable', src: 'src/assets/global/scripts/datatable.js' },
    { name: 'datatable-plugin', src: 'src/assets/global/plugins/datatables/datatables.min.js' },
    { name: 'daterangepicker', src: 'src/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js' },
    //{ name: 'datatables.bootstrap', src: 'src/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' },

    { name: 'layout', src: 'src/assets/layouts/layout/scripts/layout.js' },
    { name: 'demo', src: 'src/assets/layouts/layout/scripts/demo.js' },
    { name: 'quick-sidebar', src: 'src/assets/layouts/global/scripts/quick-sidebar.js' },
];
