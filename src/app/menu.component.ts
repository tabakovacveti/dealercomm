import { Component } from '@angular/core';

import { ScriptService } from './script.service';

@Component({
    selector: 'menu-app',
    templateUrl: '/app.menu.component.html'
})

export class MenuComponent {
    pageTitle: string = 'Dealer Commissioning';
   
    scriptService: ScriptService = new ScriptService();

    constructor() {    
    }

    
    selectedItem: string = "welcome";

    selectMenu(menuItem: string): void {

        this.selectedItem = menuItem;

    }
}
