import { DealerCommPage } from './app.po';

describe('dealer-comm App', () => {
  let page: DealerCommPage;

  beforeEach(() => {
    page = new DealerCommPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
